clc
clear all

% Signal without noise
t = 0:.001:.25;
x = 5*sin(2*pi*50*t) + 7*sin(2*pi*120*t);

T = t(2) - t(1);            % Sample time
Fs = 1/T;                   % Sampling frequency
L =  size(x,2);             % Length of signal
t = (0:L-1)*T;              % Time vector

NFFT = 2^nextpow2(L);       % Next power of 2 from length of y
f = Fs/2*linspace(0,1,NFFT/2+1);

% Added noise
y = x + 0.5*randn(size(t));
figure
plot(y)
title('Noisy time domain signal')

Y = fft(y,NFFT)/L;

Pyy = 2*abs(Y);
%f = 1000/251*(0:127);
figure
plot(f,Pyy(1:NFFT/2+1))
title('Power spectral density')
xlabel('Frequency (Hz)')