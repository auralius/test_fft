function [f_, Py_] = my_fft(t, signal)

T = t(2) - t(1);            % Sampling time
Fs = 1/T;                   % Sampling frequency
L =  size(signal,2);        % Length of signal

NFFT = 2^nextpow2(L);       % Next power of 2 from length of y
f = Fs/2*linspace(0,1,NFFT/2+1);

Y = fft(signal, NFFT)/L;

Py = 2*abs(Y);

f_ = f(1:NFFT/2+1);
Py_ = Py(1:NFFT/2+1);


end