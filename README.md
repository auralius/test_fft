# How to use MATLAB FFT function

MATLAB has an fft function. However, how to use it is never straight forward. 
I have simplified the cumbersome steps of using MATLAB fft function into one reusable function.